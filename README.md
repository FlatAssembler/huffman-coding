# Huffman coding

A JavaScript program that implements the Huffman algorithm and calculates entropy of a string of characters. You can see it live here: https://flatassembler.github.io/huffman.html